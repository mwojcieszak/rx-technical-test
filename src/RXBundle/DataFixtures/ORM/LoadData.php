<?php
namespace MT\APIBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RXBundle\Entity\Product;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $products = ['Mars', 'Snickers', 'Twix', 'Price Polo'];

        foreach ($products as $element) {
            $product = new Product();
            $product->setName($element);

            $manager->persist($product);
        }

        $manager->flush();

    }

    public function getOrder()
    {
        return 50;
    }
}