<?php

namespace RXBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;


class ShoppingCartService extends ContainerAware
{
    private $sessionKey = 'shoppingCart';

    public function setProduct($product)
    {
        $products = $this->container->get('session')->get($this->sessionKey) ? $this->container->get('session')->get($this->sessionKey) : [];

        array_push($products, $product);

        $this->container->get('session')->set($this->sessionKey, $products);
    }

    public function getProducts()
    {
        return $this->container->get('session')->get($this->sessionKey);
    }

    public function clear()
    {
        $this->container->get('session')->set($this->sessionKey, null);
    }
}