<?php

namespace RXBundle\Controller;

use RXBundle\Entity\CartHistory;
use RXBundle\Form\ProductListType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $shoppingCartService = $this->get('rx.shoppingCart');

        $products = $em->getRepository('RXBundle:Product')->findAll();

        $form = $this->createForm(new ProductListType());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productName = $form->get('item')->getData()->getName();
            $shoppingCartService->setProduct($productName);

            $cartHistory = new CartHistory();
            $cartHistory->setName($productName);
            $em->persist($cartHistory);

            $em->flush();
        } else {
            $shoppingCartService->clear();
        }

        return $this->render('RXBundle:Default:index.html.twig', ['products' => $products, 'form' => $form->createView(), 'shoppingCart' => $shoppingCartService->getProducts()]);
    }

    public function cartAction()
    {
        return $this->render('RXBundle:Default:cart.html.twig', ['cart' => $this->get('rx.shoppingCart')->getProducts()]);
    }

    /**
     * @Route("/cart-clear", name="cart_clear")
     */
    public function clearCartAction()
    {
        $this->get('rx.shoppingCart')->clear();

        return $this->redirect('/');
    }
}
