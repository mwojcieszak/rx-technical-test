Redexpert's technical test
========================

It's my Symfony technical test

Project based on Symfony 2.8


Installation
------------

### Add your user to www-data group

### Create database and define config/parameters.yml

### Build databases
    php app/console doctrine:schema:update --force

### Load fixtures
    php app/console doctrine:fixtures:load

### Run composer to get all dependencies
    composer install

### Install bootstrap
    bower install bootstrap

### Run server
    php app/console server:run